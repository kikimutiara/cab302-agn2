import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.geom.Point2D;
import java.util.LinkedList;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;

public class DrawLine {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI(){
        DrawingAreas drawingAreas = new DrawingAreas();
        ButtonPanel buttonPanel = new ButtonPanel(drawingAreas);

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Draw on Component");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(drawingAreas);
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    static class ButtonPanel extends JPanel implements ActionListener{
        private DrawingAreas drawingAreas;

        public ButtonPanel(DrawingAreas drawingAreas){
            this.drawingAreas = drawingAreas;

            add(createButton("     ", Color.BLACK));
            add(createButton("     ", Color.RED));
            add(createButton("     ", Color.YELLOW));
            add(createButton("     ", Color.BLUE));
//            add(createButton("     ", Color.GREEN));
            add(createButton("Clear Drawing", null));
        }

        private JButton createButton(String text, Color background){
            JButton button = new JButton(text);
            button.setBackground(background);
            button.addActionListener(this);

            return button;
        }

        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();

            if("Clear Drawing".equals(e.getActionCommand())){
                drawingAreas.clear();
            } else {
                drawingAreas.setForeground(button.getBackground());
            }
        }
    }

    static class DrawingAreas extends JComponent {
        private final static int AREA_SIZE = 400;
        private LinkedList<ColoredLine> ColoredLines = new LinkedList<ColoredLine>();
        private Line2D line;

        public DrawingAreas(){
            setBackground(Color.WHITE);

            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
            addMouseMotionListener(ml);
        }

        @Override
        public Dimension getPreferredSize(){
            return isPreferredSizeSet() ?
                    super.getPreferredSize() : new Dimension(AREA_SIZE, AREA_SIZE);
        }

        @Override
        protected void paintComponent(Graphics g){
            super.paintComponent(g);

            Color foreground = g.getColor();

            g.setColor(Color.BLACK);
            g.drawString("TESTING LINE LIKE RECTANGLE", 40, 15);

            for (DrawingAreas.ColoredLine cr : ColoredLines){
                g.setColor( cr.getForeground());
                Line2D l = cr.getLine();
                int x1 = (int) (l.getP1().getX());
                int y1 = (int) (l.getP1().getY());
                int x2 = (int) (l.getP2().getX());
                int y2 = (int) (l.getP2().getY());

                g.drawLine(x1, y1, x2, y2);
            }

            if (line != null){
                Graphics2D g2d = (Graphics2D)g;
                g2d.setColor(foreground);
                g2d.draw(line);
            }
        }

        public void addLine(Line2D line, Color color){
            ColoredLine cr = new ColoredLine(color, line);
            ColoredLines.add(cr);
            repaint();
        }

        public void clear(){
            ColoredLines.clear();
            repaint();
        }

        class MyMouseListener extends MouseInputAdapter{

            int x1;
            int x2;
            int y1;
            int y2;
            private Point startPoint;
            public Point2D w1;
            public Point2D w2;

            public void mousePressed(MouseEvent e){
                startPoint = e.getPoint();
                line = new Line2D.Double();
                w1 = e.getPoint();

//                x1 = startPoint.x;
//                y1 = startPoint.y;
            }

            public void mouseDragged(MouseEvent e){

//                x1 = startPoint.x;
//                y1 = startPoint.y;
//                x2 = startPoint.x;
//                y2 = startPoint.y;
                w2 = e.getPoint();

//                line.setLine(x1, y1, x2, y2);
                line.setLine(w1, w2);
//                line.getBounds();
                paintComponent(getGraphics());
                repaint();
            }

            public void mouseReleased(MouseEvent e){
                if (line.getY1() != 0 || line.getY2() != 0){
                    addLine(line, e.getComponent().getForeground());
                }

                line = null;
            }
        }

        class ColoredLine{
            private Color foreground;
            private Line2D line;

            public ColoredLine(Color foreground, Line2D line){
                this.foreground = foreground;
                this.line = line;
            }

            public Color getForeground(){
                return foreground;
            }

            public void setForeground(Color foreground){
                this.foreground = foreground;
            }

            public Line2D getLine(){
                return line;
            }
        }
    }
}