import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Path2D;
import java.util.ArrayList;

public class DrawPolygon {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }


    // for creating and showing the GUI of the project
    private static void createAndShowGUI(){
        DrawingArea drawingArea = new DrawingArea();
        ButtonPanel buttonPanel = new ButtonPanel(drawingArea);

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Draw On Component");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(drawingArea);
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        buttonPanel.setBackground(Color.LIGHT_GRAY);
        frame.setSize(400,400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    // class for making the button of the project and event listener as well
    static class ButtonPanel extends JPanel implements ActionListener {
        private DrawingArea drawingArea;

        //create Button
        public ButtonPanel(DrawingArea drawingArea){
            this.drawingArea = drawingArea;

            add(createButton("     ", Color.BLACK));
            add(createButton("     ", Color.RED));
            add(createButton("     ", Color.YELLOW));
            add(createButton("     ", Color.BLUE));
            add(createButton("     ", Color.GREEN));
            add(createButton("Clear Drawing", null));
        }

        // function for creating button
        private JButton createButton(String text, Color background){
            JButton button = new JButton(text);
            button.setBackground(background);
            button.addActionListener(this);

            return button;
        }

        // action for getting the foreground of the button or clear drawing canvas
        public void actionPerformed(ActionEvent e){
            JButton button = (JButton)e.getSource();

            if("Clear Drawing".equals(e.getActionCommand())){
                drawingArea.clear();
            } else {
                drawingArea.setForeground(button.getBackground());
            }
        }
    }

    // class for drawing the Polygon Shape
    static class DrawingArea extends JComponent {
        private final static int AREA_SIZE = 400;
        private ArrayList<ColoredPolygon> ColoredPolygons = new ArrayList<ColoredPolygon>();
        private Path2D polygon;

        // constructor for Drawing class and the mouse listener called
        public DrawingArea(){
            setBackground(Color.WHITE);

            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
            addMouseMotionListener(ml);
        }

        @Override
        public Dimension getPreferredSize(){
            return isPreferredSizeSet() ?
                    super.getPreferredSize() : new Dimension(AREA_SIZE, AREA_SIZE);
        }

        // painting component
        @Override
        protected void paintComponent(Graphics g){
            super.paintComponent(g);

            Color foreground = g.getColor();

            g.setColor(Color.BLACK);
            g.drawString("TESTING POLYGON LIKE RECTANGLE", 40,15);

            for (DrawingArea.ColoredPolygon cp : ColoredPolygons){
                g.setColor(cp.getForeground());
                polygon = cp.getPolygon();
                Graphics2D g2d = (Graphics2D)g;
                g2d.draw(polygon);

            }
        }

        public void addPolygon(Path2D polygon, Color color){
            ColoredPolygon cp = new ColoredPolygon(color, polygon);
            ColoredPolygons.add(cp);
//            repaint();
        }

        public void clear(){
            ColoredPolygons.clear();
            repaint();
        }

        class MyMouseListener extends MouseInputAdapter {
            ArrayList<Double> loc = new ArrayList<Double>();
            private Path2D polygon;

            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);

                loc.add((double) e.getX()); // add the location of the mouse pointer X axis
                loc.add((double) e.getY()); // add the location of the mouse pointer Y axis

                Double x = (loc.get(loc.size() - 2)); // get the last position of the X axis which is second to last index
                Double y = (loc.get(loc.size() - 1)); // get the last position of the Y axis which is the last index

                // this code is the one that showing error, i think when i swapped both of them, so the LineTo func is
                // called first, it will show an error about missing initial moveTo Path.
                // if moveTo called first, we will not have error but it will not draw either.

                if(loc.size() == 2){
                    polygon = new Path2D.Double();
                    addPolygon(polygon, e.getComponent().getForeground());
                    polygon.moveTo(x, y);
                }
                repaint();
                polygon.lineTo(x, y);

                if(loc.get(0) >= (loc.get(loc.size() - 2) - 10) && loc.get(0) <= (loc.get(loc.size() - 2) + 10)
                        && loc.get(1) >= (loc.get(loc.size() - 1) - 10) &&  loc.get(1) <= (loc.get(loc.size() - 1) + 10)
                        && loc.size() > 2){
                    polygon.closePath();
                    polygon = null;
                    loc.clear();
                }



//                // this is the problem about the null exception
//                if (x != 0 || y != 0){
//                    addPolygon(polygon, e.getComponent().getForeground());
//                } else {
//                    polygon = null;
//                }

                System.out.println(ColoredPolygons.size());
            }
        }

        class ColoredPolygon{
            private Color foreground;
            private Path2D polygon;

            public ColoredPolygon(Color foreground, Path2D polygon){
                this.foreground = foreground;
                this.polygon = polygon;
            }

            public Color getForeground(){
                return foreground;
            }

            public void setForeground(Color foreground){
                this.foreground = foreground;
            }

            public Path2D getPolygon(){
                return polygon;
            }
        }
    }
}





















