import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class DrawEllipse {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI(){
        DrawingArea drawingArea = new DrawingArea();
        ButtonPanel buttonPanel = new ButtonPanel(drawingArea);

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Draw On Component");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(drawingArea);
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    static class ButtonPanel extends JPanel implements ActionListener{
        private DrawingArea drawingArea;

        public ButtonPanel(DrawingArea drawingArea){
            this.drawingArea = drawingArea;

            add( createButton("	", Color.BLACK) );
            add( createButton("	", Color.RED) );
            add( createButton("	", Color.GREEN) );
            add( createButton("	", Color.BLUE) );
            add( createButton("	", Color.ORANGE) );
            add( createButton("	", Color.YELLOW) );
            add( createButton("Clear Drawing", null) );
        }

        private JButton createButton(String text, Color background){
            JButton button = new JButton(text);
            button.setBackground(background);
            button.addActionListener(this);

            return button;
        }

        public void actionPerformed(ActionEvent e){
            JButton button = (JButton)e.getSource();

            if("Clear Drawing".equals(e.getActionCommand())){
                drawingArea.clear();
            } else {
                drawingArea.setForeground(button.getBackground());
            }
        }
    }


    static class DrawingArea extends JComponent {
        private final static int AREA_SIZE = 400;
        private ArrayList<ColoredEllipse> ColoredEllipses = new ArrayList<ColoredEllipse>();
        private Ellipse2D ellipse;

        public DrawingArea(){
            setBackground(Color.BLACK);

            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
            addMouseMotionListener(ml);
        }

        @Override
        public Dimension getPreferredSize(){
            return isPreferredSizeSet() ?
                    super.getPreferredSize() : new Dimension(AREA_SIZE, AREA_SIZE);
        }

        @Override
        protected void paintComponent(Graphics g){
            super.paintComponent(g);

            Color foreground = g.getColor();

            g.setColor(Color.BLACK);
            g.drawString("TESTING ELLIPSE LIKE RECTANGLE", 40, 15);

            for (DrawingArea.ColoredEllipse cr : ColoredEllipses){
                g.setColor(cr.getForeground());
                Ellipse2D el = cr.getEllipse();
                int x = (int) (el.getX());
                int y = (int) (el.getY());
                int width = (int) (el.getWidth());
                int height = (int) (el.getHeight());
                g.drawOval(x, y, width, height);
//                g.drawOval(x,y,1,1); // this is for PLOT or Dots

            }

            if (ellipse != null){
                Graphics2D g2d = (Graphics2D)g;
                g2d.setColor(foreground);
                g2d.draw(ellipse);
            }
        }

        public void addEllipse(Ellipse2D ellipse, Color color){
            ColoredEllipse ce = new ColoredEllipse(color, ellipse);
            ColoredEllipses.add(ce);
            repaint();
        }

        public void clear(){
            ColoredEllipses.clear();
            repaint();
        }

        class MyMouseListener extends MouseInputAdapter{
            private Point2D startPoint;

            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);

                startPoint = e.getPoint();
                ellipse = new Ellipse2D.Double();
            }

            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);

                int x = (int) Math.min(startPoint.getX(), e.getX());
                int y = (int) Math.min(startPoint.getY(), e.getY());
                int width = (int) Math.abs(startPoint.getX() - e.getX());
                int height = (int) Math.abs(startPoint.getY() - e.getY());

                ellipse.setFrame(x, y, width, height);
                repaint();
            }

            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);

                if(ellipse.getWidth() != 0 || ellipse.getHeight() != 0){
                    addEllipse(ellipse, e.getComponent().getForeground());
                }

                ellipse = null;
            }
        }

        class ColoredEllipse{
            private Color foreground;
            private Ellipse2D ellipse;

            public ColoredEllipse(Color foreground, Ellipse2D ellipse){
                this.foreground = foreground;
                this.ellipse = ellipse;
            }

            public Color getForeground(){
                return foreground;
            }

            public void setForeground(Color foreground){
                this.foreground = foreground;
            }

            public Ellipse2D getEllipse(){
                return ellipse;
            }
        }
    }
}
























