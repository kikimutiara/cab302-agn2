import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;

public class CombinedDraw{

    public enum Shapes{
        PLOT,
        LINE,
        RECTANGLE,
        ELLIPSE,
        POLYGON
    }

    public Graphics g;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI() {

        DrawingAreas drawingAreas = new DrawingAreas();
        ButtonPanel buttonPanel = new ButtonPanel(drawingAreas);

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Draw on Component");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(drawingAreas);
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        frame.setSize(615, 675);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static class ButtonPanel extends JPanel implements ActionListener{
        private DrawingAreas drawingAreas;
        private OpenFile open = new OpenFile();


        public ButtonPanel(DrawingAreas drawingAreas){
            this.drawingAreas = drawingAreas;
            add(open);
            add(createFileComboBox());
            add(createShapeComboBox());
            add(createButton("Reset", null));
            add(createButton("Change Color", null));
            add(createButton("Undo", null));
            add(createCheckBox("Fill Color"));
            try{
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }

        private JButton createButton(String text, Color background){
            JButton button = new JButton(text);
            button.setBackground(background);
            button.addActionListener(this);
            button.setFocusPainted(false);
            button.setContentAreaFilled(false);

            return button;
        }

        private JComboBox createShapeComboBox(){
            String[] shapes = {"Choose Shape", "Line", "Plot", "Ellipse", "Rectangle", "Polygon"};
            JComboBox comboBox = new JComboBox(shapes);
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JComboBox shapesCombo = (JComboBox)e.getSource();
                    String name = (String)shapesCombo.getSelectedItem();
                    if (name == "Line"){
                        drawingAreas.shapeType = Shapes.LINE;
                    } if (name == "Plot"){
                        drawingAreas.shapeType = Shapes.PLOT;
                    } if (name == "Ellipse"){
                        drawingAreas.shapeType = Shapes.ELLIPSE;
                    } if (name == "Rectangle"){
                        drawingAreas.shapeType = Shapes.RECTANGLE;
                    } if (name == "Polygon"){
                        drawingAreas.shapeType = Shapes.POLYGON;
                    } else {

                    }

                    Action undo = new AbstractAction() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(drawingAreas.DrawShapeList.size() > 0){
                                drawingAreas.DrawShapeList.removeLast();
                                drawingAreas.drawFile();
                            }

                        }
                    };

                    comboBox.getInputMap().put(KeyStroke.getKeyStroke("Z".charAt(0), InputEvent.CTRL_DOWN_MASK), "undo");
                    comboBox.getActionMap().put("undo", undo);
                }
            });

            return comboBox;
        }

        private JComboBox createFileComboBox(){
            String[] message = {"Choose Selection", "File", "Save", "Export"};
            JComboBox comboBox = new JComboBox(message);
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JComboBox d = (JComboBox)e.getSource();
                    String name = (String)d.getSelectedItem();
//                    comboBox.setEnabled(false);
                    if (name == "File"){
                        open.OpenFile();
                        drawingAreas.DrawShapeList = open.draw();
                        drawingAreas.drawFile();
                    } if (name == "Save"){
                        SaveFile save = new SaveFile(drawingAreas.DrawShapeList);
                    } if (name == "Export"){
                        ToPNG export = new ToPNG();
                        export.savePNG(drawingAreas.img);
                        JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), "Exported Completed!");
                    } else {
                    }
                    comboBox.setSelectedIndex(0);

                    Action undo = new AbstractAction() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(drawingAreas.DrawShapeList.size() > 0){
                                drawingAreas.DrawShapeList.removeLast();
                                drawingAreas.drawFile();
                            }

                        }
                    };

                    comboBox.getInputMap().put(KeyStroke.getKeyStroke("Z".charAt(0), InputEvent.CTRL_DOWN_MASK), "undo");
                    comboBox.getActionMap().put("undo", undo);
                }

            });

            return comboBox;
        }

        private JCheckBox createCheckBox(String text){
            JCheckBox checkBox = new JCheckBox(text);
            checkBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (checkBox.isSelected()){
                        drawingAreas.isChecked = true;
                    } else {
                        drawingAreas.isChecked = false;
                    }
                }
            });
            return checkBox;
        }


        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
//            JComboBox f = (JComboBox)e.getSource();
            Color colorPen = Color.black;

            Action undo = new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(drawingAreas.DrawShapeList.size() > 0){
                        drawingAreas.DrawShapeList.removeLast();
                        drawingAreas.drawFile();
                    }

                }
            };
            button.getInputMap().put(KeyStroke.getKeyStroke("Z".charAt(0), InputEvent.CTRL_DOWN_MASK), "undo");
            button.getActionMap().put("undo", undo);



            if("Reset".equals(e.getActionCommand())) {
                drawingAreas.clear();
            }
//            }
            if("Change Color".equals(e.getActionCommand())){
                Color color = JColorChooser.showDialog(this, "Choose a Color and Press OK", colorPen);
                ColorPick colorPick1 = new ColorPick(color);
                drawingAreas.setForeground(colorPick1.getColor());
            } if("Undo".equals(e.getActionCommand())){
                if(drawingAreas.DrawShapeList.size() > 0){
                    drawingAreas.DrawShapeList.removeLast();
                    drawingAreas.drawFile();
                }
            }
//            Action undo = new AbstractAction() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    if(drawingAreas.DrawShapeList.size() > 0){
//                        drawingAreas.DrawShapeList.removeLast();
//                        drawingAreas.drawFile();
//                    }
//
//                }
//            };



        }
    }

    static class ColorPick {
        private Color color;

        public ColorPick(Color color){
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    public static class Checked {
        private boolean checked = false;

        public Checked(boolean checked){
            this.checked = checked;
        }

        public boolean isChecked() {
            return checked;
        }
    }

    static class DrawingAreas extends JComponent{
        private final static int AREA_SIZE = 400;
        public LinkedList<DrawShapes> DrawShapeList = new LinkedList<DrawShapes>();
        private Shapes shapeType;
        public boolean isChecked = false;

        private BufferedImage img = new BufferedImage(AREA_SIZE + 400, AREA_SIZE + 400, BufferedImage.TYPE_INT_ARGB);
        Graphics2D imgg = (Graphics2D)img.getGraphics();

        public DrawingAreas(){
            setBackground(Color.WHITE);

            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
            addMouseMotionListener(ml);

        }

        @Override
        public Dimension getPreferredSize(){
            return isPreferredSizeSet() ?
                    super.getPreferredSize() : new Dimension(AREA_SIZE, AREA_SIZE);
        }

        @Override
        protected void paintComponent(Graphics g){

            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            Shape shape = new Path2D.Double();
            for (DrawShapes draw : DrawShapeList){
                g.setColor(draw.getPen());


                if(draw.getShapeType() == Shapes.PLOT){
                    shape = new Ellipse2D.Double(draw.getCoords().get(0), draw.getCoords().get(1), 1, 1);
                } if(draw.getShapeType() == Shapes.LINE){
                    shape = new Line2D.Double(draw.getCoords().get(0), draw.getCoords().get(1),
                            draw.getCoords().get(2), draw.getCoords().get(3));
                } if(draw.getShapeType() == Shapes.RECTANGLE){
                    shape = new Rectangle2D.Double(draw.getCoords().get(0), draw.getCoords().get(1),
                            (draw.getCoords().get(2) - draw.getCoords().get(0)),
                            (draw.getCoords().get(3) - draw.getCoords().get(1)));
                } if(draw.getShapeType() == Shapes.ELLIPSE){
                    shape = new Ellipse2D.Double(draw.getCoords().get(0), draw.getCoords().get(1),
                            (draw.getCoords().get(2) - draw.getCoords().get(0)),
                            (draw.getCoords().get(3) - draw.getCoords().get(1)));
                } if(draw.getShapeType() == Shapes.POLYGON){
                    shape = new Path2D.Double();

                    ((Path2D.Double) shape).moveTo(draw.getCoords().get(0), draw.getCoords().get(1));
                    for(int i = 2; i < draw.getCoords().size(); i+=2){
                        ((Path2D.Double) shape).lineTo(draw.getCoords().get(i), draw.getCoords().get(i+1));

                    }
                    if(draw.getCoords().get(0) >= draw.getCoords().get(draw.getCoords().size() - 2) - 10 &&
                            draw.getCoords().get(0) <= draw.getCoords().get(draw.getCoords().size() - 2) + 10 &&
                            draw.getCoords().get(1) >= draw.getCoords().get(draw.getCoords().size() - 1) - 10 &&
                            draw.getCoords().get(1) <= draw.getCoords().get(draw.getCoords().size() - 1) + 10){
                        ((Path2D.Double) shape).closePath();
                    }else{
                        ((Path2D.Double) shape).lineTo(draw.getCoords().get(0), draw.getCoords().get(1));
                        ((Path2D.Double) shape).closePath();
                    }

                }
                g2d.draw(shape);
//                imgg.translate(-AREA_SIZE, -AREA_SIZE);
                imgg.draw(shape);
                if(draw.getisFill()){
                    g.setColor(draw.getFill());
                    g2d.fill(shape);
                    repaint();
                }
            }
        }

        public void addShape(Shapes shapeType, Color pen, Color fill, boolean isFill, ArrayList<Double> coords){
            DrawShapes cr = new DrawShapes(shapeType, pen, fill, isFill, coords) ;
            DrawShapeList.add(cr);
            repaint();
        }

        public void drawFile(){
            repaint();
        }

        public void clear(){
            DrawShapeList.clear();
            repaint();
        }



        class MyMouseListener extends MouseInputAdapter{
            public Point2D startPoint;
            public Point2D w1;
            public Point2D w2;
            public ArrayList<Double> coord = new ArrayList<Double>();


            public void mousePressed(MouseEvent e){

                coord.add(e.getPoint().getX());
                coord.add(e.getPoint().getY());
            }

            public void mouseReleased(MouseEvent e){
                coord.add(e.getPoint().getX());
                coord.add(e.getPoint().getY());

                if(shapeType == Shapes.POLYGON){
                    coord.remove(coord.size() - 1);
                    coord.remove(coord.size() - 1);
                    if(coord.size() == 2){

                        addShape(shapeType, getForeground(), getForeground(), isChecked, coord);
                    }else if(coord.get(0) >= coord.get(coord.size() - 2) - 10 &&
                        coord.get(0) <= coord.get(coord.size() - 2) + 10 &&
                        coord.get(1) >= coord.get(coord.size() - 1) - 10 &&
                        coord.get(1) <= coord.get(coord.size() - 1) + 10){
                        coord = null;
                        coord = new ArrayList<Double>();
                    }
                }else{
                    addShape(shapeType, getForeground(), getForeground(), isChecked, coord);
                    coord = null;
                    coord = new ArrayList<Double>();
                }
                repaint();


            }
        }

        static class DrawShapes {


            private Shapes shapeType;
            private Color pen;
            private Color fill;
            private Boolean isFill;
            private ArrayList<Double> coords;

            public DrawShapes(Shapes shapeType, Color pen, Color fill, boolean isFIll, ArrayList<Double> coords){

                this.shapeType = shapeType;
                this.pen = pen;
                this.fill = fill;
                this.isFill = isFIll;
                this.coords = coords;
            }

            public Color getPen(){
                return pen;
            }

            public Color getFill(){
                return fill;
            }

            public boolean getisFill(){
                return isFill;
            }

            public ArrayList<Double> getCoords() {
                return coords;
            }

            public Shapes getShapeType(){
                return shapeType;
            }
        }
    }
}