import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class DrawPlot {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI(){
        DrawingArea drawingArea = new DrawingArea();
        ButtonPanel buttonPanel = new ButtonPanel(drawingArea);

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Draw On Component");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(drawingArea);
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    static class ButtonPanel extends JPanel implements ActionListener{
        private DrawingArea drawingArea;
        public ButtonPanel(DrawingArea drawingArea){
            this.drawingArea = drawingArea;

            add( createButton("	", Color.BLACK) );
            add( createButton("	", Color.RED) );
            add( createButton("	", Color.GREEN) );
            add( createButton("	", Color.BLUE) );
            add( createButton("	", Color.ORANGE) );
            add( createButton("	", Color.YELLOW) );
            add( createButton("Clear Drawing", null) );
        }

        private JButton createButton(String text, Color background){
            JButton button = new JButton(text);
            button.setBackground(background);
            button.addActionListener(this);

            return button;
        }

        public void actionPerformed(ActionEvent e){
            JButton button = (JButton)e.getSource();

            if("Clear Drawing".equals(e.getActionCommand())){
                drawingArea.clear();
            } else {
                drawingArea.setForeground(button.getBackground());
            }
        }
    }

    static class DrawingArea extends JComponent {
        private final static int AREA_SIZE = 400;
        private ArrayList<ColoredDot> ColoredDots = new ArrayList<ColoredDot>();
        private Ellipse2D dot;

        public DrawingArea(){
            setBackground(Color.BLACK);

            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
            addMouseMotionListener(ml);
        }

        @Override
        public Dimension getPreferredSize(){
            return isPreferredSizeSet() ?
                    super.getPreferredSize() : new Dimension(AREA_SIZE, AREA_SIZE);
        }

        @Override
        protected void paintComponent(Graphics g){
            super.paintComponent(g);

            Color foreground = g.getColor();

            g.setColor(Color.BLACK);
            g.drawString("TESTING DOTS LIKE RECTANGLE", 40,15);

            for (DrawingArea.ColoredDot cd : ColoredDots){
                g.setColor(cd.getForeground());
                Ellipse2D d = cd.getDot();
                int x = (int) (d.getX());
                int y = (int) (d.getY());
                g.fillOval(x, y, 10, 10);
            }

            if (dot != null){
                Graphics2D g2d = (Graphics2D)g;
                g2d.setColor(foreground);
                g2d.draw(dot);
            }
        }

        public void addDot(Ellipse2D dot, Color color){
            ColoredDot cd = new ColoredDot(color, dot);
            ColoredDots.add(cd);
            repaint();
        }

        public void clear(){
            ColoredDots.clear();
            repaint();
        }

        class MyMouseListener extends MouseInputAdapter {
            private Point2D startPoint;

            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                startPoint = e.getPoint();
                dot = new Ellipse2D.Double();

                int x = (int) Math.min(startPoint.getX(), e.getX());
                int y = (int) Math.min(startPoint.getY(), e.getY());
                dot.setFrame(x, y, 5, 5);
                repaint();

                if(dot.getWidth() != 0 || dot.getHeight() != 0){
                    addDot(dot, e.getComponent().getForeground());
                }

                dot = null;
            }

//            public void mouseClicked(MouseEvent e){
//                super.mouseClicked(e);
//
//                x = e.getPoint().x;
//                y = e.getPoint().y;
//
//                dot.setFrame(x, y, 1, 1);
////                addDot(dot, e.getComponent().getForeground());
//                repaint();
//
//                if(dot.getY() != 0){
//                    addDot(dot, e.getComponent().getForeground());
//                } else {
//                    dot = null;
//                }
//
//            }

//            @Override
//            public void mouseReleased(MouseEvent e) {
//                super.mouseReleased(e);
//
//                int x = (int) Math.min(startPoint.getX(), e.getX());
//                int y = (int) Math.min(startPoint.getY(), e.getY());
//                dot.setFrame(x, y, 5, 5);
//                repaint();
//
//                if(dot.getWidth() != 0 || dot.getHeight() != 0){
//                    addDot(dot, e.getComponent().getForeground());
//                }
//
//                dot = null;
//            }
        }

        class ColoredDot{
            private Color foreground;
            private Ellipse2D dot;

            public ColoredDot(Color foreground, Ellipse2D dot){
                this.foreground = foreground;
                this.dot = dot;
            }

            public Color getForeground(){
                return foreground;
            }

            public void setForeground(Color foreground){
                this.foreground = foreground;
            }

            public Ellipse2D getDot(){
                return dot;
            }
        }
    }
}

























