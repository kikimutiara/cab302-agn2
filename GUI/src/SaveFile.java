import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.LinkedList;

public class SaveFile {
    public SaveFile(LinkedList<CombinedDraw.DrawingAreas.DrawShapes> shapeType) {

        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("."));
        int retrieval = chooser.showSaveDialog(null);
        if (retrieval == JFileChooser.APPROVE_OPTION) {
            try {
                FileWriter fw = new FileWriter(chooser.getSelectedFile()+".vec");
                Color pen = Color.BLACK;
                Color fill = Color.WHITE;
                boolean isFill = false;
                for(CombinedDraw.DrawingAreas.DrawShapes shape : shapeType){
                    if(shape.getPen() != pen){
                        System.out.println(shape.getPen());
                        pen = shape.getPen();
                        String hex = Integer.toHexString(pen.getRGB() & 0xffffff);
                        if (hex.length() < 6) {
                            hex = "000000".substring(0, 6 - hex.length()) + hex;
                        }
                        fw.write("PEN #" + hex + "\n");
                    }
                    if(shape.getFill() != fill){
                        fill = shape.getFill();
                        String hex = Integer.toHexString(fill.getRGB() & 0xffffff);
                        if (hex.length() < 6) {
                            hex = "000000".substring(0, 6 - hex.length()) + hex;
                        }
                        if(shape.getisFill()){
                            fw.write("FILL #" + hex + "\n");
                        } else {
                            fw.write("FILL OFF\n");
                        }

                    }
                    if(shape.getShapeType() == CombinedDraw.Shapes.LINE){
                        String coords = "";
                        for (Double coord : shape.getCoords()){
                            coord /= 800;
                            coords += coord.toString() + " ";
                        }
                        fw.write("LINE " + coords + "\n");
                    }
                    if(shape.getShapeType() == CombinedDraw.Shapes.PLOT){
                        String coords = "";
                        for(int i = 0; i < 2; i++){
                            Double coord =  shape.getCoords().get(i);
                            coord /= 800;
                            coords += coord.toString() + " ";
                        }
                        fw.write("PLOT "+ coords + "\n");
                    }
                    if(shape.getShapeType() == CombinedDraw.Shapes.ELLIPSE){
                        String coords = "";
                        for(Double coord : shape.getCoords()){
                            coord /= 800;
                            coords += coord.toString() + " ";
                        }
                        fw.write("ELLIPSE " + coords + "\n");
                    }
                    if(shape.getShapeType() == CombinedDraw.Shapes.RECTANGLE){
                        String coords = "";
                        for(Double coord : shape.getCoords()){
                            coord /= 800;
                            coords += coord.toString() + " ";
                        }
                        fw.write("RECTANGLE " + coords + "\n");
                    }
                    if(shape.getShapeType() == CombinedDraw.Shapes.POLYGON){
                        String coords = "";
                        for(Double coord : shape.getCoords()){
                            coord /= 800;
                            coords += coord.toString() + " ";
                        }
                        fw.write("POLYGON " + coords + "\n");
                    }
                }
//                fw.write(sb);
                fw.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
