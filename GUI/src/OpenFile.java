import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

public class OpenFile extends JComponent{
    private String path = null;
    private static double y_span = 52;
    private String line;
    private String fill = "";
    private Shape shape = null;
    private BufferedReader reader;

    public void OpenFile() {


        JFileChooser jfc = new JFileChooser();
        jfc.setCurrentDirectory(new File("."));
        jfc.addChoosableFileFilter(new FileNameExtensionFilter("VEC Files", "vec"));
        int result = jfc.showOpenDialog(this);

        if (result == JFileChooser.APPROVE_OPTION) {
            File file = jfc.getSelectedFile();
            path = file.getAbsolutePath();
        }

        try{
            reader = new BufferedReader(new FileReader(path));
            line = reader.readLine();
        } catch(IOException e1){
            e1.printStackTrace();
            System.out.println(e1);
        }



    }
    public LinkedList<CombinedDraw.DrawingAreas.DrawShapes> draw(){
        final int AREA_SIZE = 600;
        System.out.println("lala");
        Color pen = Color.BLACK;
        Color fill = Color.WHITE;
        boolean isFill = false;
        ArrayList<Double> coords;
        LinkedList<CombinedDraw.DrawingAreas.DrawShapes> shapeList = new LinkedList<CombinedDraw.DrawingAreas.DrawShapes>();
        try {
            while (line != null) {
                String[] words = line.split("\\s+");

                if(words[0].equals("PEN")){
                    pen = Color.decode(words[1]);
                }
                if(words[0].equals("FILL")){
                    if (!words[1].equals("OFF")){
                        fill = Color.decode(words[1]);
                        isFill = true;
                    }else {
                        isFill = false;
                    }


                }

                if(words[0].equals("LINE")) {
                    double x1 = Double.parseDouble(words[1]) * AREA_SIZE;
                    double y1 = Double.parseDouble(words[2]) * AREA_SIZE;
                    double x2 = Double.parseDouble(words[3]) * AREA_SIZE;
                    double y2 = Double.parseDouble(words[4]) * AREA_SIZE;
                    coords = new ArrayList<Double>();
                    coords.add(x1);
                    coords.add(y1);
                    coords.add(x2);
                    coords.add(y2);
                    shapeList.add(new CombinedDraw.DrawingAreas.DrawShapes(CombinedDraw.Shapes.LINE,
                            pen, fill, isFill, coords));
                }
                if(words[0].equals("ELLIPSE")){

                    double x1 = Double.parseDouble(words[1]) * AREA_SIZE;
                    double y1 = Double.parseDouble(words[2]) * AREA_SIZE;
                    double x2 = Double.parseDouble(words[3]) * AREA_SIZE;
                    double y2 = Double.parseDouble(words[4]) * AREA_SIZE;
                    coords = new ArrayList<Double>();
                    coords.add(x1);
                    coords.add(y1);
                    coords.add(x2);
                    coords.add(y2);
                    shapeList.add(new CombinedDraw.DrawingAreas.DrawShapes(CombinedDraw.Shapes.ELLIPSE,
                            pen, fill, isFill, coords));
                }
                if(words[0].equals("RECTANGLE")){
                    double x1 = Double.parseDouble(words[1]) * AREA_SIZE;
                    double y1 = Double.parseDouble(words[2]) * AREA_SIZE;
                    double x2 = Double.parseDouble(words[3]) * AREA_SIZE;
                    double y2 = Double.parseDouble(words[4]) * AREA_SIZE;
                    coords = new ArrayList<Double>();
                    coords.add(x1);
                    coords.add(y1);
                    coords.add(x2);
                    coords.add(y2);
                    shapeList.add(new CombinedDraw.DrawingAreas.DrawShapes(CombinedDraw.Shapes.RECTANGLE,
                            pen, fill, isFill, coords));
                }
                if(words[0].equals("PLOT")){
                    double x1 = Double.parseDouble(words[1]) * AREA_SIZE;
                    double y1 = Double.parseDouble(words[2]) * AREA_SIZE;
                    coords = new ArrayList<Double>();
                    coords.add(x1);
                    coords.add(y1);
                    shapeList.add(new CombinedDraw.DrawingAreas.DrawShapes(CombinedDraw.Shapes.PLOT,
                            pen, fill, isFill, coords));
                }
                if(words[0].equals("POLYGON")){
                    coords = new ArrayList<Double>();
                    coords.add(Double.parseDouble(words[1]) * AREA_SIZE);
                    coords.add(Double.parseDouble(words[2]) * AREA_SIZE);
                    for (int i = 3; i < words.length - 1; i += 2){
                        coords.add(Double.parseDouble(words[i]) * AREA_SIZE);
                        coords.add(Double.parseDouble(words[i+1]) * AREA_SIZE);
                    }
                    shapeList.add(new CombinedDraw.DrawingAreas.DrawShapes(CombinedDraw.Shapes.POLYGON,
                            pen, fill, isFill, coords));
                }
                line = reader.readLine();
            }
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        return shapeList;
    }


}
