//import java.awt.image.BufferedImage;
//
//public class FileOpener {
//
//    public static void main(String[] args) {
//        ImageEditEngine program = new ImageEditEngine();
//
//    }
//
//
//
//
//        import java.io.File;
//
//        import javax.imageio.ImageIO;
//        import javax.swing.ImageIcon;
//
//    public class ImageEditEngine {
//
//        private static int[][][] originalImage;
//        private static BufferedImage originalImageBuffer;
//
//        public ImageEditEngine() {
//            originalImage = new int[0][0][0];
//            ImageGUI gui = new ImageGUI();
//            gui.setVisible(true);
//        }
//
//        public static ImageIcon openImage(String filepath) {
//            try {
//
//                File f = new File(filepath);
//                BufferedImage image = ImageIO.read(f);
//                originalImageBuffer = image;
//                ImageIcon icon = new ImageIcon(image);
//                return icon;
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                return new ImageIcon();
//            }
//
//        }
//    }
//
//        import java.awt.Dimension;
//        import java.awt.BorderLayout;
//        import java.awt.event.ActionEvent;
//        import java.awt.event.ActionListener;
//        import java.io.File;
//        import javax.swing.ImageIcon;
//        import javax.swing.JFileChooser;
//        import javax.swing.JFrame;
//        import javax.swing.JLabel;
//        import javax.swing.JMenu;
//        import javax.swing.JMenuBar;
//        import javax.swing.JMenuItem;
//        import javax.swing.JPanel;
//        import javax.swing.UIManager;
//
//    public class ImageGUI extends JFrame implements ActionListener {
//
//        private JFrame window;
//
//        private JMenuItem mOpenFile;
//        private JMenuItem mSaveFile;
//        private JMenuItem mExitCommand;
//
//        private JMenuBar parentBar;
//        private JMenu fileMenu;
//        private JMenu commandMenu;
//
//        private JPanel mainPanel;
//
//        public ImageIcon img;
//
//        private JLabel mLabel;
//
//        public ImageGUI() {
//            super();
//            try {
//                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            //setup CommandMenu
//            commandMenu = makeMenu("Command", 'C');
//
//            //setup FileMenu
//            fileMenu = makeMenu("File", 'F');
//            mOpenFile = makeMenuItem(fileMenu, "Open...", 'O');
//            mSaveFile = makeMenuItem(fileMenu, "Save...", 'S');
//            mExitCommand = makeMenuItem(fileMenu, "Exit", 'X');
//
//            //setup parentBar
//            parentBar = new JMenuBar();
//            parentBar.add(fileMenu);
//            parentBar.add(commandMenu);
//
//            //main panel
//            mainPanel = new JPanel(new BorderLayout());
//            mLabel = new JLabel();
//            mainPanel.add(mLabel, BorderLayout.CENTER);
//            setJMenuBar(parentBar);
//            setSize(new Dimension(400, 300));
//
//        }
//
//        protected JMenuItem makeMenuItem(JMenu menu, String name, char mnemonic) {
//            JMenuItem m = new JMenuItem(name, (int) mnemonic);
//            m.addActionListener(this);
//            menu.add(m);
//            return m;
//        }
//
//        protected JMenu makeMenu(String name, char mnemonic) {
//            JMenu menu = new JMenu(name);
//            menu.setMnemonic(mnemonic);
//            return menu;
//        }
//
//
//        @Override
//        public void actionPerformed(ActionEvent arg0) {
//            if (arg0.getSource() == mOpenFile) {
//                String path = null;
//
//                JFileChooser jfc = new JFileChooser();
//                jfc.setCurrentDirectory(new File("."));
//
//                int result = jfc.showOpenDialog(this);
//
//                if (result == JFileChooser.APPROVE_OPTION) {
//                    File file = jfc.getSelectedFile();
//                    path = file.getAbsolutePath();
//                }
//
//                img = ImageEditEngine.openImage(path);
//                mLabel = new JLabel(img);
//                mLabel.setVisible(true);
//                this.repaint();
//                pack();
//
//            }
//
//        }
//    }
//}